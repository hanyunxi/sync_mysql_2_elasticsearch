

## 前言

#### - 项目模块

###### BinlogMiddleware

1、binlog中间件，负责解析binlog，把变动的数据以json形式发送到kafka队列。

###### KafkaMiddleware

2、kafka中间件，负责消费kafka队列中的Message，把数据写入Elasticsearch中。

#### - 基础服务

###### （1）Mysql

###### （2）Kafka（用于存放mysql变动消息，存放于Kafka队列）

###### （3）Elasticsearch

ES搜索引擎查询效率高，mysql数据可以实时同步到ES，可以大幅度提高搜索的并发和数据时时性。

## 1、开启mysql、mariadb的binlog

#### （1）binlog简介

binlog,即二进制日志,它记录了数据库上的所有改变，并以二进制的形式保存在磁盘中；
 它可以用来查看数据库的变更历史、数据库增量备份和恢复、Mysql的复制（主从数据库的复制）。

**行模式和语句模式的区别**
1.语句模式：
100万条记录
只需1条delete * from test；就可以删除100万条记录
2.row模式
100万条记录
记录100万条删除命令

#### （2）Binary Log记录方式

###### Row Level (行模式)

Binary Log会记录成**每一行数据被修改的形式**，然后在Slave端再对相同的数据进行修改。
 如果修改了表的结构，那么binlog日志记录的是重新创建表，在插入字段、update等操作语句，而不是的alter的动作。
 优点：在Row Level模式下，Binnary Log可以不记录执行的Query语句的上下文相关信息，只要记录哪一行修改了，修改成什么样子。Row Level会详细的记录下每一行数据的修改细节，而且不会出现某个特定情况下的存储过程，或Function，以及Trigger的调用和触发无法被正确复制问题。
 缺点：产生大量的日志内容。

###### Statment Level **（默认） **（语句模式）

**每一条会修改的SQL语句都会记录**到Master的Binnary log中。Slave端在复制的时候，SQL线程会解析成和原来Master端执行过相同的SQL语句，并再次执行。
 优点：首先，解决了Row Level下的缺点，不须要记录每一行的数据变化，减少了Binnary Log日志量，节约了IO成本，提高了性能。
 缺点：由于它是记录的执行语句，为了让这些语句在Slave端也能正确执行。那么它还必须记录每条语句在执行时的一些相关信息，即上下文信息，以保证所有语句在Slave端被执行的时候能够得到和在Master端执行时相同的结果。另外，由于MySQL发展比较快，很多新功能不断加入，使得MySQL复制遇到了不小的挑战，复制时设计的内容岳父在，越容易出bug。在Statement Level下，目前已发现不少的情况下会造成MySQL的复制问题。主要是在修改数据使用了某些特定的函数功能后，出现，比如：Sleep()函数在有些版本中就不能正确的复制，在存储过程中使用了last_insert_id()函数，可能会使Slave和Master的到不一致的ID，等等。

###### Mixed Level

在Mixed模式下，MySQL会根据执行的每一条具体的SQL语句来区分对待记录的日志形式，也就是在Statement和Row之间选择一种。除了MySQL认为通过Statement方式可能造成复制过程中Master和Slave之间产生不一致数据。(如特殊Procedure和Funtion的使用，UUID()函数的使用等特殊情况)时，它会选择ROW的模式来记录变更之外，都会使用Statement方式。
 ps：在后续开发中将使用Row格式

**企业场景如何选择binlog模式**
1、互联网公司，使用MySQL的功能相对少（存储过程、触发器、函数）
选择默认的语句模式，Statement Level（默认）
2、公司如果用到使用MySQL的特殊功能（存储过程、触发器、函数）
则选择Mixed模式
3、公司如果用到使用MySQL的特殊功能（存储过程、触发器、函数）又希望数据最大化一直，此时最好选择Row level模式

#### （3）开启mysql的binlog

mariadb 开启binlog  配置文件名称叫 mariadb.cnf (/etc/mysql/mariadb.cnf)

```
[client-server]
# Port or socket location where to connect
# port = 3306
socket = /run/mysqld/mysqld.sock

# Import all .cnf files from configuration directory
[mariadbd]
skip-host-cache
skip-name-resolve

datadir=/var/lib/mysql
log-error=/var/log/mysqld.log
pid-file=/var/run/mysqld/mysqld.pid
log_bin=/var/lib/mysql/binlog
log_bin_index=/var/lib/binlog/mysql-bin.index
server-id=1
binlog_format=row

!includedir /etc/mysql/mariadb.conf.d/
!includedir /etc/mysql/conf.d/

```

 启动mariadb

```
docker run -itd --name mariadb -v /hy/data/mariadb/data/mariadb.cnf:/etc/mysql/mariadb.cnf -v /hy/data/mariadb/data:/var/lib/mysql  -p 3306:3306 -e MYSQL_ROOT_PASSWORD=123456 mariadb
```



```mysql
# 查看是否开启binlog日志，log_bin为ON表示开启
mysql> show variables like '%log_bin%';
+---------------------------------+-----------------------------------------+
| Variable_name                   | Value                                   |
+---------------------------------+-----------------------------------------+
| log_bin                         | ON                                      |
| log_bin_basename                | /mariadb/data/bin-log/mariadb-log       |
| log_bin_compress                | OFF                                     |
| log_bin_compress_min_len        | 256                                     |
| log_bin_index                   | /mariadb/data/bin-log/mariadb-log.index |
| log_bin_trust_function_creators | OFF                                     |
| sql_log_bin                     | ON                                      |
+---------------------------------+-----------------------------------------+

#查看binlog模式
show global variables like '%binlog_format%';
binlog_format,ROW
wsrep_forced_binlog_format,NONE

```



### 2、Binlog日志生产消息到Kafka

```dockerfile
version: "3"
services:
  zookeeper:
    image: 'bitnami/zookeeper:latest'
    ports:
      - '2181:2181'
    environment:
      - ALLOW_ANONYMOUS_LOGIN=yes
  kafka:
    image: 'bitnami/kafka:latest'
    ports:
      - '9092:9092'
    environment:
      - KAFKA_BROKER_ID=1
      - KAFKA_CFG_LISTENERS=PLAINTEXT://:9092
      - KAFKA_CFG_ADVERTISED_LISTENERS=PLAINTEXT://127.0.0.1:9092
      - KAFKA_CFG_ZOOKEEPER_CONNECT=zookeeper:2181
      - ALLOW_PLAINTEXT_LISTENER=yes
    depends_on:
      - zookeeper
```

```dockerfile
启动：docker-compose up -d
关闭：docker-compose down
```

BinlogMiddleware服务主要负责监听Binlog日志，并将其发送到Kafka队列（及Kafka生产者）。

- 本示例模拟监听teemoliu数据库的user、role表。为了方便表结构设计的很简单，均只含有id、name两个属性。

- 中间件写进Kafka队列的消息格式如下：

  ```json
  {"event":"SJAnti-fraud.user.insert","value":[5,"wangqi"]}
  {"event":"SJAnti-fraud.user.update","value":[5,"wangqi2222"]}
  ```

  #### 导入mave依赖(note: springboot  和 kafka 版本冲突问题 )

```xml
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.kafka</groupId>
            <artifactId>spring-kafka</artifactId>
        </dependency>
		<!--版本冲突  异常-->
        <dependency>
            <groupId>org.apache.kafka</groupId>
            <artifactId>kafka-clients</artifactId>
            <version>2.5.0</version>
        </dependency>

        <dependency>
            <groupId>com.github.shyiko</groupId>
            <artifactId>mysql-binlog-connector-java</artifactId>
            <version>0.16.1</version>
        </dependency>

        <dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>fastjson</artifactId>
            <version>1.2.49</version>
        </dependency>
```





### 3、消费Kafka消息同步数据到ES

```dockerfile
从容器中复制文件到宿主机：
docker cp elasticsearch:/usr/share/elasticsearch/data /hy/data/es/data

docker cp elasticsearch:/usr/share/elasticsearch/logs /hy/data/es/data

docker cp elasticsearch:/usr/share/elasticsearch/config/elasticsearch.yml /hy/data/es/data/elasticsearch.yml


docker run -d --name=elasticsearch \
  --restart=always \
  -p 9200:9200 -p 9300:9300 \
  -e "discovery.type=single-node" \
  -v /hy/data/es/data/elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml \
  -v /hy/data/es/data/data:/usr/share/elasticsearch/data \
  -v /hy/data/es/data/logs:/usr/share/elasticsearch/logs \
  elasticsearch
```

api

```
http://192.168.1.99:9200/_all
http://192.168.1.99:9200/_search
```



