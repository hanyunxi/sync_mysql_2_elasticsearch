package com.hy.sync.binlog.binlog;

import com.alibaba.fastjson.JSON;
import com.github.shyiko.mysql.binlog.BinaryLogClient;
import com.github.shyiko.mysql.binlog.event.*;
import com.hy.sync.binlog.domain.Binlog;
import com.hy.sync.binlog.kafka.KafkaSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * binlog监听 BinlogClientRunner
 */
@EnableAsync
@Component
public class BinlogClientRunner implements CommandLineRunner {
    @Value("${binlog.host}")
    private String host;

    @Value("${binlog.port}")
    private int port;

    @Value("${binlog.user}")
    private String user;

    @Value("${binlog.password}")
    private String password;

    // binlog server_id
    @Value("${server.id}")
    private long serverId;

    // kafka话题
    @Value("${kafka.topic}")
    private String topic;

    // kafka分区
    @Value("${kafka.partNum}")
    private int partNum;

    // Kafka备份数
    @Value("${kafka.repeatNum}")
    private short repeatNum;

    // kafka地址
    @Value("${spring.kafka.bootstrap-servers}")
    private String kafkaHost;

    // 指定监听的数据表
    @Value("${binlog.database.table}")
    private String database_table;

    @Autowired
    private KafkaSender kafkaSender;

    @Async
    @Override
    public void run(String... args) throws Exception {
        // 创建topic
        kafkaSender.createTopic(kafkaHost,topic,partNum,repeatNum);
        // 获取监听数据表数组
        List<String> databaseList = Arrays.asList(database_table.split(","));
        HashMap<Long, String> tableMap = new HashMap<Long, String>();
        // 创建binlog监听客户端
        BinaryLogClient client = new BinaryLogClient(host, port, user, password);
        client.setServerId(serverId);
        client.registerEventListener((event -> {
            EventData data = event.getData();
            if (data != null){
                if (data instanceof TableMapEventData){
                    TableMapEventData tableMapEventData = (TableMapEventData) data;
                    tableMap.put(tableMapEventData.getTableId(),tableMapEventData.getDatabase()+"."+tableMapEventData.getTable());
                }
                // update data
                if (data instanceof UpdateRowsEventData){
                    UpdateRowsEventData updateRowsEventData = (UpdateRowsEventData) data;
                    String tableName = tableMap.get(updateRowsEventData.getTableId());
                    if (tableName != null && databaseList.contains(tableName)){
                        String eventKey = tableName+".update";
                        for (Map.Entry<Serializable[], Serializable[]> row : updateRowsEventData.getRows()) {
                            String msg = JSON.toJSONString(new Binlog(eventKey, row.getValue()));
                            System.out.println("push update message ---->:"+msg);
                            kafkaSender.sendMessage(topic,msg);
                        }
                    }
                }
                // insert
                else if (data instanceof WriteRowsEventData){
                    WriteRowsEventData writeRowsEventData = (WriteRowsEventData) data;
                    String tableName = tableMap.get(writeRowsEventData.getTableId());
                    if (tableName != null && databaseList.contains(tableName)){
                        String eventKey = tableName+".insert";
                        for (Serializable[]  row : writeRowsEventData.getRows()) {
                            String msg = JSON.toJSONString(new Binlog(eventKey, row));
                            System.out.println("push insert message ---->:"+msg);
                            kafkaSender.sendMessage(topic,msg);
                        }
                    }
                }
                // delete
                else if (data instanceof DeleteRowsEventData){
                    DeleteRowsEventData deleteRowsEventData = (DeleteRowsEventData) data;
                    String tableName = tableMap.get(deleteRowsEventData.getTableId());
                    if (tableName != null && databaseList.contains(tableName)){
                        String eventKey = tableName+".delete";
                        for (Serializable[]  row : deleteRowsEventData.getRows()) {
                            String msg = JSON.toJSONString(new Binlog(eventKey, row));
                            System.out.println("push delete message ---->:"+msg);
                            kafkaSender.sendMessage(topic,msg);
                        }
                    }
                }

            }


        }));

        // 连接
        client.connect();
    }



}
