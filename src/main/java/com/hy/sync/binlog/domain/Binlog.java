package com.hy.sync.binlog.domain;


public class Binlog {
    private String event;
    private Object value;

    public Binlog(String event, Object value) {
        this.event = event;
        this.value = value;
    }

    public Binlog() {
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
}
