package com.hy.sync;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
public class SyncMysql2ElasticsearchApplication {

    public static void main(String[] args) {
        SpringApplication.run(SyncMysql2ElasticsearchApplication.class, args);
    }

}
